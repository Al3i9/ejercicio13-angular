import { Component, OnInit } from '@angular/core';
import { Ipersona } from 'src/app/interfaces/persona.interface';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  persona: Ipersona = {
    correo: ''
  };

  constructor() { }

  ngOnInit(): void {
  }

  guardar(): void {
    console.log(this.persona.correo);
        
  }

}
